const todosKey = 'todos';

export const saveTodos = (todos = []) => {
  localStorage.setItem(todosKey, JSON.stringify(todos));
};

export const loadTodos = () => {
  const todos = localStorage.getItem(todosKey);

  if (todos) {
    return JSON.parse(localStorage.getItem(todosKey));
  }

  return [];
};
