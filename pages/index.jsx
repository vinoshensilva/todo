// Import libraries
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

// Import components
import Todos from '@/components/organism/todos/index';

// Import actions
import { setTodos } from '@/reducers/todos';

// Import functions
import { loadTodos } from '@/functions/storage';

export default function Home() {
  const dispatch = useDispatch();

  useEffect(() => {
    const todos = loadTodos();

    dispatch(setTodos({ todos }));
  }, []);

  return (
    <div className="container">
      <Todos />
    </div>
  );
}
