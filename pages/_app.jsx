import '../styles/globals.css';

// Import libraries
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { store } from '@/store/index';

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

MyApp.propTypes = {
  pageProps: PropTypes.shape({}).isRequired,
  Component: PropTypes.elementType.isRequired,
};

export default MyApp;
