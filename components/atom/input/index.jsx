import React from 'react';
import PropTypes from 'prop-types';

function Input({
  value,
  onChange,
  ...props
}) {
  return (
    <input
      type="text"
      value={value}
      onChange={onChange}
      {...props}
    />
  );
}

Input.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Input;
