// Import libraries
import React from 'react';
import PropTypes from 'prop-types';

function Button({
  children,
  ...props
}) {
  return (
    <button
      type="button"
      {...props}
    >
      {children}
    </button>
  );
}

Button.defaultProps = {
  children: null,
};

Button.propTypes = {
  children: PropTypes.node,
};

export default Button;
