// Import libraries
import React from 'react';
import { useSelector } from 'react-redux';

// Import components
import Todo from '@/components/molecule/todo/index';
import AddTodo from '@/components/molecule/addtodo/index';

function Todos() {
  const todos = useSelector((state) => state.todosReducer.todos);

  if (todos.length < 1) {
    return (
      <div className="todos">
        <div className="text-center">
          <AddTodo />
        </div>
        <div>
          No todos currently.
        </div>
      </div>
    );
  }

  return (
    <div className="todos">
      <AddTodo />
      {
        todos.map((todo, index) => {
          const {
            done,
            text,
          } = todo;

          return (
            <Todo
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              text={text}
              done={done}
              index={index}
            />
          );
        })
      }
    </div>
  );
}

export default Todos;
