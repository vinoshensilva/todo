// Import libraries
import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

// Import components
import Input from '@/components/atom/input/index';
import Button from '@/components/atom/button/index';

// Import actions
import {
  deleteTodo,
  setTodoDone,
  setTodoText,
} from '@/reducers/todos';

function Todo({ text, index, done }) {
  const dispatch = useDispatch();

  const onChange = (e) => {
    const { value } = e.target;

    dispatch(setTodoText({ index, text: value }));
  };

  const onClickRemove = () => {
    dispatch(deleteTodo({ index }));
  };

  const onClickDone = () => {
    dispatch(setTodoDone({ index }));
  };

  return (
    <div className="todo">
      <Input
        value={text}
        onChange={onChange}
      />
      <div className="todos__btns">
        <Button
          onClick={onClickDone}
          className="todos__donebtn"
        >
          {
            done
              ? (
                <svg xmlns="http://www.w3.org/2000/svg" style={{ width: '1.25em', height: '1.25em' }} fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M5 13l4 4L19 7" />
                </svg>
              ) : (
                <svg xmlns="http://www.w3.org/2000/svg" style={{ width: '1.25em', height: '1.25em' }} fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                </svg>
              )
          }
        </Button>
        <Button
          onClick={onClickRemove}
          className="todos__removebtn"
        >
          <svg xmlns="http://www.w3.org/2000/svg" style={{ width: '1.25em', height: '1.25em' }} viewBox="0 0 20 20" fill="currentColor">
            <path fillRule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clipRule="evenodd" />
          </svg>
        </Button>
      </div>
    </div>
  );
}

Todo.propTypes = {
  index: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  done: PropTypes.bool.isRequired,
};

export default Todo;
