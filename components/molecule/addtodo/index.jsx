// Import libraries
import React from 'react';
import { useDispatch } from 'react-redux';

// Import components
import Button from '@/components/atom/button/index';

// Import actions
import { addTodo } from '@/reducers/todos';

function AddTodo() {
  const dispatch = useDispatch();

  const onClick = () => {
    dispatch(addTodo());
  };

  return (
    <Button
      onClick={onClick}
      className="todos__addbtn"
    >
      <div className="addbtn__text">
        Add Todo
      </div>
    </Button>
  );
}

export default AddTodo;
