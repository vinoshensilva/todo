/* eslint-disable no-param-reassign */
import { saveTodos } from '@/functions/storage';
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  todos: [],
};

const todoSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {
    addTodo: (state) => {
      state.todos.push({
        text: 'Do this',
        done: false,
      });

      saveTodos(state.todos);
    },
    setTodoText: (state, action) => {
      const { index, text } = action.payload;

      state.todos[index].text = text;

      saveTodos(state.todos);
    },
    setTodos: (state, action) => {
      const { todos } = action.payload;

      state.todos = todos;
    },
    deleteTodo: (state, action) => {
      const { index } = action.payload;

      state.todos.splice(index, 1);

      saveTodos(state.todos);
    },
    setTodoDone: (state, action) => {
      const { index } = action.payload;

      state.todos[index].done = !state.todos[index].done;

      saveTodos(state.todos);
    },
  },
});

export const {
  addTodo,
  deleteTodo,
  setTodoDone,
  setTodoText,
  setTodos,
} = todoSlice.actions;

export default todoSlice.reducer;
